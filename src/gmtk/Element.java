package gmtk;

import processing.core.*;
import java.util.ArrayList;
import java.util.List;

public class Element extends Object {

		Object parent;

		public Element(Object p, Rect r) {
			super(p.app, r);
			parent = p;
			parent.lo.add(this);
		}

		public Element(Object p, int x, int y, int w, int h) {
			this(p, new Rect(x, y, w, h));
		}

		public Rect getPosition() {
			Rect pp = parent.getPosition();
			Rect result = new Rect(pp.x + rect.x, pp.y + rect.y, rect.w, rect.h);
			return result;
		}

		public void draw() {}

		public void relativeCenterX(float Xpercent) {
			rect.x = (int)((float)parent.rect.x + (float)parent.rect.w / 100 * Xpercent - (float)rect.w/2);
		}

		public void relativeCenterY(float Ypercent) {
			rect.y = (int)((float)parent.rect.y + (float)parent.rect.h / 100 * (float)Ypercent - (float)rect.h/2);
		}

		public void relativeCenter(int Xpercent, int Ypercent) {
			relativeCenterX(Xpercent);
			relativeCenterY(Ypercent);
		}

		void kill() {
			clean();
			parent.lo.remove(this);
		}
}
