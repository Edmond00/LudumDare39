package gmtk;

import processing.core.*;
import java.util.ArrayList;
import java.util.List;

public class Thunder {

	public enum State {
		STRUCK(),
		WAIT();

		State() {};
	}

	public class Thunderbolt {
		int time;
		int x;

		Thunderbolt(int time) {
			this.x = -1;
			this.time = time;
		}
	}

	static int stomachFirst = 5;
	int stomach;

	int countdown;
	int struckCountdown;
	int waitLifetime;
	int struckLifetime;
	int waitSpeed;
	int struckSpeed;
	ArrayList<Thunderbolt> thunderbolts;
	State state;

	Thunder() {
		stomach = stomachFirst;
		waitLifetime = 500;
		struckLifetime = 30;
		waitSpeed = 2;
		struckSpeed = 1;
		thunderbolts = new ArrayList<Thunderbolt>();
		reset();
	}

	void struck() {
		Ground.thunder(thunderbolts);
		struckCountdown -= struckSpeed;
		if (struckCountdown <= 0)
			reset();
	}

	void die() {
		stomach = stomachFirst;
		struckCountdown = 0;
		GameScreen.himself.reset();
		Game.app.main.setGameOver(true);
		Game.app.changeScreen("main");
		reset();
	}

	void waiting() {
		if (Ground.avatar.died) {
			die();
			return;
		}
		countdown -= waitSpeed;
		for (Thunderbolt tb : thunderbolts) {
			if (tb.x == -1 && countdown < tb.time) {
				tb.x = Ground.here;
				Game.app.sounds.play("mark");
			}
		}
		if (countdown <= 0) {
			if (stomach > 0) 
				stomach--;
			for (Thunderbolt tb : thunderbolts) {
				if (tb.x == -1)
					tb.x = Ground.here;
			}
			state = State.STRUCK;
			struckCountdown = struckLifetime;
			Game.app.sounds.play("thunder");
		}
	}

	void update() {
		if (state == State.STRUCK)
			struck();
		else
			waiting();
	}

	void eat(int value) {
		Game.app.sounds.play("eat");
		stomach += value;
	}

	void drawLighting(int X, int index, int absoluteX) {
		int rand = (int)Game.app.random(0, 1000);
		if (rand % 2 == 0) {
			Game.app.background(Color.SHADOW);
		}
		int step = rand % 5 + 4;
		int x, y, x2, y2;
		x2 = X; 
		y2 = 0; 

		int rand2 = (int)Game.app.random(0, 1000);
		int dir = (rand % 2 == 0) ? 1 : -1;
		int v = 100;
		int w = step + 2;
		int sky = Game.app.screen.h - Ground.heigth;
		for (int i = 1; i < step; i++) {
			x = x2;
			y = y2;
			y2 = y + sky / step;
			x2 = X + (dir * (rand2 % v));
			rand2 = (int)Game.app.random(0, 1000);
			dir = (rand2 % 2 == 0) ? 1 : -1;
			Game.app.drawLine(x, y, x2, y2, w, Color.THUNDER);
			w--;
		}
		x = x2;
		y = y2;
		y2 = sky;
		x2 = X;
		Game.app.drawLine(x, y, x2, y2, w, Color.THUNDER);
	}

	void drawThunder() {
		int h = Game.app.screen.h;
		int x;
		int i = 0;
		for (Thunderbolt tb : thunderbolts) {
			i++;
			x = Ground.getPositionX(tb.x);
			drawLighting(x, i, tb.x);
			//Game.app.drawMark(x, 0, h, Color.THUNDER);
		}
	}

	void drawStomach() {
		String str = Integer.toString(stomach);
		Game.app.centerText(str, 20, Game.app.screen.w/2, 30, Color.TITLE);
	}

	void drawCountdown() {
		int marge = 100;
		int border = 7;
		int x, y, w, h;
		w = Game.app.screen.w - (marge*2);
		h = 20;
		x = marge;
		y = 50;
		Rect r = new Rect(x, y, w, h);
		Game.app.drawRect(r, Color.COUNTDOWN_BORDER);
		r.x += border;
		r.y += border;
		r.w -= border*2;
		r.h -= border*2;
		int length = r.w;
		Game.app.drawRect(r, Color.COUNTDOWN_BACK);
		r.w = (int)((double)r.w / (double)waitLifetime * (double)(waitLifetime - countdown));
		Game.app.drawRect(r, Color.COUNTDOWN_FRONT);
		r.y -= border;
		r.h += border*2;
		int mx;
		for (Thunderbolt tb : thunderbolts) {
			mx = r.x + (int)((double)length / (double)waitLifetime * (double)(waitLifetime - tb.time));
			if (tb.x == -1)
				Game.app.drawMark(mx, r.y, r.h, Color.COUNTDOWN_MARK);
			
		}
	}

	void draw() {
		drawCountdown();
		drawStomach();
		if (state == State.STRUCK)
			drawThunder();
	}

	void reset() {
		state = State.WAIT;
		thunderbolts.clear();
		countdown = waitLifetime;
		int r = (int)Game.app.random(1, 4);
		int time;
		for (int i = 0; i < r; i++) {
			time = (int)Game.app.random(50, waitLifetime - 50);
			if (stomach <= 0)
				time = 0;
			thunderbolts.add(new Thunderbolt(time));
		}
	}

}

