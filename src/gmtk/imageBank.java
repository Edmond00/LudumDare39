package gmtk;

import java.util.Hashtable;
import processing.core.*;

public class ImageBank {
	
	Hashtable<String, PImage> bank;

	ImageBank() {
		bank = new Hashtable<String, PImage>();
		add("move1");
		add("move2");
		add("move3");
		add("move4");
		add("rmove1");
		add("rmove2");
		add("rmove3");
		add("rmove4");
		add("swim1");
		add("swim2");
		add("rswim1");
		add("rswim2");
		add("rabbit1");
		add("rabbit2");
		add("rrabbit1");
		add("rrabbit2");
		add("fish");
		add("rfish");
		add("herb");
		add("tree");
		add("flower");
		add("bherb");
		add("btree");
		add("bflower");
		add("birch");
		add("bbirch");
		add("cloud");
		add("ash");
		add("pig1");
		add("pig2");
		add("rpig1");
		add("rpig2");
		add("pine");
		add("bpine");
		add("rose");
	}

	void add(String file) {
		PImage tmp = Game.app.loadImage("./data/img/" + file + ".png");
		bank.put(file, tmp);
	}

	void addLink(String link, String key) {
		PImage img = bank.get(key);
		if (img == null)
			return ;
		bank.put(link, img);
	}

	PImage get(String key) {
		return bank.get(key);
	}
}

