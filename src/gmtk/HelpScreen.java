package gmtk;

import processing.core.*;
import java.util.ArrayList;
import java.util.List;

public class HelpScreen extends Screen {

		List<Text> text;

		public HelpScreen(Game a, String n) {
			super(a, n, true);

			text = new ArrayList<Text>();

			int y = 10;
			int gap = 3;
			int size = 15;

			text.add(new Text(this, 50, y, "HELP:", size, Color.TITLE));
			y += gap;
			text.add(new Text(this, 50, y, "", size, Color.TITLE));
			y += gap;
			text.add(new Text(this, 50, y, "Write help here.", size, Color.TITLE));
		}

}
