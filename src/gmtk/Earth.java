package gmtk;

import processing.core.*;
import java.util.LinkedList;
import java.util.List;

public class Earth extends Ground {

		static Integer min = 400;
		static Integer max = 800;

		void addPig() {
			int r = random(-2, 3);
			int x, y;
			for (int i = 0; i < r; i++) {
				x = random(0, width);
				y = 0;
				new Pig(this, x, y);

			}
		}

		void addRabbit() {
			int r = random(-1, 7);
			int x, y;
			for (int i = 0; i < r; i++) {
				x = random(0, width);
				y = 0;
				new Rabbit(this, x, y);

			}
		}

		public Earth(Direction dir) {
			super(dir, min, max);
			name = "earth";
			addPlants();
			addRabbit();
			addPig();
		}

		void addPlant(String name, String burnName) {
			PImage img = Game.app.bank.get(name);
			int w = img.width/2;
			int x = random(w, width - w);
			new Plant(this, x, name, burnName, w);
		}

		void addPlants() {
			int n = random(width / 25, width / 10);
			int r;
			for (int i = 0; i < n; i++) {
				r = random(0, 100);
				if (r < 50)
					addPlant("herb", "bherb");
				else if (r < 57)
					addPlant("birch", "bbirch");
				else if (r < 65)
					addPlant("tree", "btree");
				else if (r < 75)
					addPlant("pine", "bpine");
				else if (r < 90)
					addPlant("flower", "bflower");
				else
					addPlant("rose", "bflower");
			}
		}

		public void processDraw(Rect rect) {
			Game.app.drawRect(rect, Color.EARTH3);
			Rect tmp = new Rect(rect.x, rect.y, rect.w, 3);
			Game.app.drawRect(tmp, Color.GROUND);
			tmp.y += tmp.h;
			tmp.h = 10;
			Game.app.drawRect(tmp, Color.EARTH1);
			tmp.y += tmp.h;
			tmp.h = 30;
			Game.app.drawRect(tmp, Color.EARTH2);
		}

		boolean deadRabbit(Item item, int x) {
			 if (!item.is("rabbit"))
			 	return false;
			return Ground.contact(item.getX(), x);
		}

		boolean deadPig(Item item, int x) {
			 if (!item.is("pig"))
			 	return false;
			return Ground.contact(item.getX(), x, 19);
		}

		boolean struck(int x) {
			if (x > begin + 5 && x < end -5)
				new Ash(this, x - begin);
			for (int i=0; i<items.size(); i++) {
				Item item = items.get(i);
				if (deadRabbit(item, x)) {
					new Food(item);
				}
				if (deadPig(item, x)) {
					int tmp;
					for (int j = 0; j < 3; j++) {
						tmp = item.x + (int)Game.app.random(0, 40) - 20;
						new Food(item, tmp);
					}
				}
				item.struck(x);
				
			}
			items.removeIf(item -> deadRabbit(item, x));
			items.removeIf(item -> deadPig(item, x));
			return true;
		}
}

