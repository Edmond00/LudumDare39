package gmtk;

import processing.core.*;

public class Particle {
		
		int x;
		int y;
		int vx;
		int vy;
		int L;
		int lifetime;
		Color color;

		public Particle(int x, int y, int vx, int vy, int L, Color c, int lifetime) {
			this.x = x;
			this.y = y;
			this.vx = vx;
			this.vy = vy;
			this.L = L;
			this.color = c;
			this.lifetime = lifetime;
		}

		void kill() {
			Ground.particles.remove(this);
		}
		
		void draw() {
			int px = Ground.getPositionX(x);
			int py = Ground.getPositionY(y);
			Game.app.drawRect(px, py, L, color);
			x += vx;
			y += vy;
			if (lifetime > 0)
				lifetime--;
			else
				kill();
			if (py < 0 && vy <= 0)
				kill();
			if (py > Game.app.screen.h && vy >= 0)
				kill();
		};
}

