package gmtk;

import processing.core.*;
import java.util.ArrayList;
import java.util.List;

public class Window extends Element {

		boolean selected;

		public Window(Object p, int x, int y, int w, int h) {
			super(p, x, y, w, h);
			selected = false;
		}

		public void draw() {
			if (!selected) {
				app.strokeWeight(2);
				app.stroke(Color.BORDER.R, Color.BORDER.G, Color.BORDER.B);
				app.fill(Color.WINDOW.R, Color.WINDOW.G, Color.WINDOW.B);
				app.rect(rect.x, rect.y, rect.w, rect.h);
			} else {
				app.strokeWeight(2);
				app.stroke(Color.SBORDER.R, Color.SBORDER.G, Color.SBORDER.B);
				app.fill(Color.SWINDOW.R, Color.SWINDOW.G, Color.SWINDOW.B);
				app.rect(rect.x, rect.y, rect.w, rect.h);
			}
			
		}
}


