package gmtk;

import processing.core.*;
import java.util.LinkedList;
import java.util.List;

public class Water extends Ground {

		static Integer min = 200;
		static Integer max = 500;

		void addFish() {
			int r = random(-1, 4);
			int x, y;
			for (int i = 0; i < r; i++) {
				x = random(10, width - 10);
				y = random(20, heigth - 10);
				new Fish(this, x, y);

			}
		}

		public Water(Direction dir) {
			super(dir, min, max);
			name = "water";
			addFish();
		}

		int getWaveY(int i) {
			int v = 12;
			int time = (i + (Game.app.millis() / 150)) % v;
			if (time > v/2)
				time = (v/2) - (time % (v/2));
			int result = time - v/3;
			return result;
		}

		public void processDraw(Rect rect) {
//			Game.app.drawRect(rect, Color.WATER);
			int wave = 18;
			int i = 0;
			Rect tmp = new Rect(rect.x, rect.y, wave, 5);
			while (tmp.x < rect.x + rect.w) {
				tmp.y = rect.y + getWaveY(i);
				tmp.h = 5;
				if (tmp.x + tmp.w > rect.x + rect.w)
					tmp.w = rect.x + rect.w - tmp.x;
				Game.app.drawRect(tmp, Color.WATER1);
				tmp.y += tmp.h;
				tmp.h = 10;
				Game.app.drawRect(tmp, Color.WATER2);
				tmp.y += tmp.h;
				tmp.h = 50;
				Game.app.drawRect(tmp, Color.WATER3);
				tmp.y += tmp.h;
				tmp.h = Game.app.screen.h - tmp.y;
				Game.app.drawRect(tmp, Color.WATER4);


				tmp.x += wave;
				i++;
			}
		}

		boolean struck(int x) {
			if (!inGroundStrict(x))
				return true;
			if (inHere == this) {
				Ground.avatar.died = true;
				return false;
			}
			for (int i=0; i<items.size(); i++) {
				Item item = items.get(i);
				if (item.is("fish")) {
					new Food(item);
				}
			}
			items.removeIf(item -> item.is("fish"));
			return true;
		}

}

