package gmtk;

import processing.core.*;

abstract public class Item {
		
		Integer x;
		Integer y;
		Ground ground;
		String name;
		boolean eaten;

		void struck(int x) {}
		boolean is(String str) {
			return str.equals(name);
		}

		int getPositionX() {
			return ground.getPositionX(x + ground.begin);
		}

		int getPositionY() {
			return ground.getPositionY(y);
		}

		int getX() {
			return ground.begin + x;
		}

		public Item(Ground g, int x, int y) {
			this.ground = g;
			g.items.add(this);
			this.x = x;
			this.y = y;
			eaten = false;
		}
		
		abstract void draw();
		void update() {};
}
