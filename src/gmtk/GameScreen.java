package gmtk;

import processing.core.*;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

public class GameScreen extends Screen {

		static GameScreen himself = null;

		public GameScreen(Game a, String n) {
			super(a, n, false);
			himself = this;
			reset();

		}

		public void reset() {
			Ground.map.clear();
			Ground.here = 0;
			Ground.index = 0;
			Ground.avatar.died = false;
			for (int i = 0; i < 5; i++) {
				Ground.grow(Ground.Direction.LEFT);
				Ground.grow(Ground.Direction.RIGHT);
			}
		}

		public void draw() {
			app.background(10,10,100);
			int step = 50;
			int vr = 3;
			int vg = 0;
			int vb = 5;
			int r = 0;
			int g = 0;
			int b = 50;
			Rect tmp = new Rect(0, 0, Game.app.screen.w, step);
			while(tmp.y + tmp.h < Game.app.screen.h) {
				Game.app.drawRect(tmp, r, g, b);
				r += vr;
				if (r > 255) r = 255;
				r += vr;
				if (r > 255) r = 255;
				g += vg;
				if (g > 255) g = 255;
				b += vb;
				if (b > 255) b = 255;
				tmp.y += tmp.h;
			}
			Ground.beginDraw();
			Ground.drawAvatar();
			update();
		}

		public boolean codedEvent(int key) {
			if (key == app.RIGHT) {
				Ground.move(Ground.Direction.RIGHT);
				Ground.avatar.state = Avatar.State.MOVE_RIGHT;
			} else if (key == app.LEFT) {
				Ground.move(Ground.Direction.LEFT);
				Ground.avatar.state = Avatar.State.MOVE_LEFT;
			}
			return true;
		}

		void update() {
			Ground.beginUpdate();
		}
}

