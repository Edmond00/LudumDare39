package gmtk;

import processing.core.*;

public class Cloud extends Item {
		
		public Cloud(Ground g, int x, int y) {
			super(g, x, y);
			name = "cloud";
		}
		
		void draw() {
			PImage img = Game.app.bank.get("cloud");
			int px = getPositionX() - img.width/2;
			int py = getPositionY() - img.height/2;
			Game.app.image(img, px, py);
		};
		void update() {};
}
