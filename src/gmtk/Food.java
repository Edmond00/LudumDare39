package gmtk;

import processing.core.*;

public class Food extends Item{


		int value;

		public Food(Item origin, int x) {
			super(origin.ground, x, origin.y);
			y = getY();
			name = "food";
			value = 1;
		}

		public Food(Item origin) {
			super(origin.ground, origin.x, origin.y);
			y = getY();
			name = "food";
			value = 1;
		}
		
		void draw() {
			Game.app.drawRect(getPositionX(), getPositionY(), 10, Color.FOOD);
		}

		int getY() {
			int v = 20;
			int time = (Game.app.millis() / 150) % v;
			if (time > v/2)
				time = (v/2) - (time % (v/2));
			int result = 0 - time;
			return result;
		}

		void update() {
			y = getY();
			if (eaten == false && Ground.contact(Ground.here, getX())) {
				Ground.thunder.eat(value);
				eaten = true;
			}
		}
}
