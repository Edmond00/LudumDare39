package gmtk;

import processing.core.*;
import java.util.ArrayList;
import java.util.List;

abstract class Object {

		Game app;
		List<Object> lo;
		Rect rect;
		boolean hide;
		boolean printChild;

		public Object(Game a, Rect r){
			app = a;
			app.lo.add(this);
			lo = new ArrayList<Object>();
			rect = r;
			hide = false;
			printChild = true;
		}

		public Rect getPosition() {
			return rect;
		}

		public Object(Game a, int x, int y, int w, int h){
			this(a, new Rect(x, y, w, h));
		}

		abstract void draw();

		public void drawComposant(){
			if (hide)
				return ;
			draw();
			if (printChild) {
				for(Object obj : lo) {
					obj.drawComposant();
				}
			}
		}

		public void transmitCodedEvent(int key) {
			if (hide)
				return ;
			if (!codedEvent(key)) return ;
			for(Object obj : lo) {
				obj.transmitCodedEvent(key);
			}
		}

		public void transmitAsciiEvent(char key) {
			if (hide)
				return ;
			if (!asciiEvent(key)) return ;
			for(Object obj : lo) {
				obj.transmitAsciiEvent(key);
			}
		}

		public void transmitUpdate() {
			update();
			for(Object obj : lo) {
				obj.transmitUpdate();
			}
		}

		void clean() {
			for (Object child : lo) {
				child.clean();
			}
			lo.clear();
		}


		boolean codedEvent(int key) {return true;}
		boolean asciiEvent(char key) {return true;}
		void update() {}

}
