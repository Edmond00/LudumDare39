
package gmtk;

import processing.core.*;

public class Rabbit extends Animal{

		public Rabbit(Ground g, int x, int y) {
			super(g, x, y);
			vitesse = 4;
			name = "rabbit";
			move = new Animation(8);
			move.add("rabbit1");
			move.add("rabbit2");
			rmove = new Animation(8);
			rmove.add("rrabbit1");
			rmove.add("rrabbit2");
			rstay = new Animation(8);
			rstay.add("rrabbit1");
			stay = new Animation(8);
			stay.add("rabbit1");
		}
		
		void draw() {
//			Game.app.drawRect(getPositionX(), getPositionY(), 10, Color.RABBIT);
			drawAnimation();
		}
}

