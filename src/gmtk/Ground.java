package gmtk;

import processing.core.*;
import java.util.LinkedList;
import java.util.ArrayList;
import java.util.List;
import java.util.ListIterator;

abstract public class Ground {

		public enum Direction {
			LEFT(),
			RIGHT();

			Direction() {}
		}

		static Thunder thunder= new Thunder();
		static Integer here = 0;
		static Avatar avatar= new Avatar(here);
		static LinkedList<Ground> map = new LinkedList<Ground>();
		static ArrayList<Particle> particles = new ArrayList<Particle>();

		static Ground inHere = null;
		static Integer index = 0;

		static Integer contactMarge = 20;
		static Integer heigth = 100;
		static Integer maxWidth = 7000;
		static Integer minWidth = 2000;
		static Integer speed = 10;
		static Integer swimSpeed = 4;
		static int avatarW = 10;
		static int avatarH = 30;

		Integer begin, end, width;
		boolean toDisplayCloud;
		String name;
		ArrayList<Item> items;

		static void move(Direction dir) {
			if (avatar.died) return;
			int s = speed;
			if (avatar.swimming())
				s = swimSpeed;
			if (dir == Direction.LEFT) {
				here -= s;
				if (here < inHere.begin) {
					index--;
					inHere = map.get(index);
				}
			}
			if (dir == Direction.RIGHT) {
				here += s;
				if (here > inHere.end) {
					index++;
					inHere = map.get(index);
				}
			}
			handleMapSize();
		}

		static void handleMapSize() {
			Ground leftBorder = map.getFirst();
			Ground rightBorder = map.getLast();
			if (here - leftBorder.begin > maxWidth) {
				map.removeFirst();
				index--;
			}
			if (rightBorder.end - here > maxWidth)
				map.removeLast();
			if (here - leftBorder.begin < minWidth)
				grow(Direction.LEFT);
			if (rightBorder.end - here < minWidth)
				grow(Direction.RIGHT);
		}

		boolean is(String str) {
			return str.equals(name);
		}

		static int random(int min, int max) {
			return (int)Game.app.random(min, max);
		}

		static void grow(Direction dir) {
			int r = random(0, 100);
			Ground adj;
			Ground g;
			if (map.size() == 0) {
				g = new Earth(dir);
				return ;
			}
			if (dir == Direction.LEFT)
				adj = map.getFirst();
			else
				adj = map.getLast();
			if (r < 70 && !adj.is("water"))
				g = new Water(dir);
			else
				g = new Earth(dir);
		}

		public Ground(Direction dir, Integer min, Integer max) {
			items = new ArrayList<Item>();
			toDisplayCloud = false;
			if (map.size() <= 0) {
				begin = 0;
				end = random(min, max);
				map.addLast(this);
				inHere = this;
			}
			else if (dir == Direction.LEFT) {
				Ground adj = map.getFirst();
				end = adj.begin;
				begin = end - random(min, max);
				map.addFirst(this);
				index++;
			} else {
				Ground adj = map.getLast();
				begin = adj.end;
				end = begin + random(min, max);
				map.addLast(this);
			}
			width = end - begin;
			addClouds();
		}

		void addCloud() {
			int x = random(0, width);
			int upBorder = -1 * (Game.app.screen.h - heigth);
			int y;
			int r = random(0, 100);
			if (r < 30)
				y = random(upBorder -100, upBorder + 100);
			else if (r < 60) 
				y = random(upBorder -100, upBorder + 150);
			else
				y = random(upBorder -100, upBorder + 200);
			new Cloud(this, x, y);
		}

		void addClouds() {
			int n = random(width / 20, width / 10);
			for (int i = 0; i < n; i++) {
				addCloud();
			}
		}

		static int getPositionX(int x) {
			int middle = Game.app.screen.w / 2;
			return x - here + middle; 
		}

		static int getPositionY(int y) {
			return y  + Game.app.screen.h - heigth;
		}

		public Rect getRect() {
			int y = Game.app.screen.h - heigth;
			int x = getPositionX(begin);
			return new Rect(x, y, end - begin, heigth);
		}

		abstract public void processDraw(Rect rect);
		public void processUpdate() {};

		public void processDrawHere(Rect rect) {
			Game.app.drawRect(rect, Color.HERE);
		}

		public void drawItems() {
			toDisplayCloud = true;
			for (Item item : items) {
				if (!item.is("cloud"))
					item.draw();
			}
		}

		public void drawClouds() {
			for (Item item : items) {
				if (item.is("cloud"))
					item.draw();
			}
		}

		static public void displayClouds() {
			for (Ground g : map) {
				if (g.toDisplayCloud)
					g.drawClouds();
				g.toDisplayCloud = false;
			}
		}

		static public void draw(Direction dir, Ground gr, ListIterator<Ground> it) {
			Rect rect = gr.getRect();
			gr.processDraw(rect);
			gr.drawItems();
			int endX = rect.x + rect.w;
			if (dir == Direction.LEFT && rect.x > 0 && it.hasPrevious())
				draw(dir, it.previous(), it);
			if (dir == Direction.RIGHT &&  endX < Game.app.screen.w && it.hasNext())
				draw(dir, it.next(), it);
		}

		static public void drawParticles() {
			for (int i =0; i < particles.size(); i++) {
				particles.get(i).draw();
			}
		}

		static public void addRains() {
			int vx = 0;
			int vy = 8;
			int x, y;
			int upBorder = -1 * (Game.app.screen.h - heigth);
//			Game.app.log("number particles : " + particles.size());
			if (particles.size() < 10000) {
				int r = random(5, 10);
				for (int i = 0 ; i < r; i++) {
					x = random(here - 800, here + 800);
					y = random(upBorder, upBorder + 100);
					particles.add(new Particle(x, y, vx, vy, 2, Color.RAIN, 500));
				}
				if (particles.size() > 400) {
					r = random(0, 5);
					for (int i = 0 ; i < r; i++) {
						x = random(here - 500, here + 500);
						y = 0;
						vx = random(0, 2) - 1;
						vy = random(1, 3) * -1;
						particles.add(new Particle(x, y, vx, vy, 3, Color.RAIN2, 8));
					}
				}
			}
		}

		static public void beginDraw() {

			drawParticles();
			Rect rect = inHere.getRect();
			ListIterator<Ground> toRight = map.listIterator(index);
			ListIterator<Ground> toLeft = map.listIterator(index);
			if (toLeft.hasPrevious())
				draw(Direction.LEFT, toLeft.previous(), toLeft);
			if (toRight.hasNext())
				draw(Direction.RIGHT, toRight.next(), toRight);
			displayClouds();
//			inHere.processDrawHere(rect);
			inHere.processDraw(rect);
			inHere.drawItems();
			thunder.draw();
		}

		static public void update(Direction dir, Ground gr, ListIterator<Ground> it) {
			Rect rect = inHere.getRect();
			gr.processUpdate();
			gr.updateItems();
			int endX = rect.x + rect.w;
			if (dir == Direction.LEFT && rect.x > 0 && it.hasPrevious())
				update(dir, it.previous(), it);
			if (dir == Direction.RIGHT &&  endX < Game.app.screen.w && it.hasNext())
				update(dir, it.next(), it);
		}

		static public void beginUpdate() {

			addRains();
			ListIterator<Ground> toRight = map.listIterator(index);
			ListIterator<Ground> toLeft = map.listIterator(index);
			if (toLeft.hasPrevious())
				update(Direction.LEFT, toLeft.previous(), toLeft);
			if (toRight.hasNext())
				update(Direction.RIGHT, toRight.next(), toRight);
			inHere.processUpdate();
			thunder.update();
			avatar.update();
		}

		public void updateItems() {
			for (Item item : items) {
				item.update();
			}
			items.removeIf(item -> item.eaten);
		}

		static public void drawAvatar() {
			avatar.draw();
/*			int x = Game.app.screen.w/2 - avatarW/2;
			int y = Game.app.screen.h - heigth - avatarH;
			Rect r = new Rect(x, y, avatarW, avatarH);
			Game.app.drawRect(r, Color.AVATAR);*/
		}

		boolean inGround(int x) {
			if (x >= begin-contactMarge && x < end + contactMarge)
				return true;
			return false;
		}

		boolean inGroundStrict(int x) {
			if (x >= begin && x < end)
				return true;
			return false;
		}

		static boolean contact(int x1, int x2) {
			int d =  x1 - x2;
			if ( d < 0) d *= -1;
			return d < contactMarge;
		}

		static boolean contact(int x1, int x2, int marge) {
			int d =  x1 - x2;
			if ( d < 0) d *= -1;
			return d < marge;
		}

		abstract boolean struck(int x);

		static public void thunder(ArrayList<Thunder.Thunderbolt> listTb) {
			if (avatar.died) return;
			for (Thunder.Thunderbolt tb : listTb) {
				if (contact(here, tb.x)) {
					avatar.died = true;
					return;
				}
				for (Ground g : map) {
					if (g.inGround(tb.x)) {
						if (!g.struck(tb.x))
							return;
					}
				}
			}
		}
}
