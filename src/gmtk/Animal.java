
package gmtk;

import processing.core.*;

abstract public class Animal extends Item{

		public enum State {
			MOVE_LEFT(),
			MOVE_RIGHT(),
			STAY_LEFT(),
			STAY_RIGHT();

			State() {}
		}

		Animation move;
		Animation rmove;
		Animation stay;
		Animation rstay;
		State state;
		int vitesse;
		
		public Animal(Ground g, int x, int y) {
			super(g, x, y);
			if (Game.app.random(0, 100) >= 50)
				state = State.MOVE_LEFT;
			else
				state = State.MOVE_RIGHT;
		}

		void drawAnimation() {
			int x = getPositionX();
			int y = getPositionY();
			if (state == State.MOVE_LEFT)
				rmove.XcenterYdown(x, y);
			if (state == State.MOVE_RIGHT)
				move.XcenterYdown(x, y);
			if (state == State.STAY_LEFT)
				rstay.XcenterYdown(x, y);
			if (state == State.STAY_RIGHT)
				stay.XcenterYdown(x, y);
		}

		void update() {
			if (Game.app.random(0, 100) < 5) {
				if  (state == State.MOVE_LEFT) 
					state = State.STAY_LEFT;
				else if  (state == State.MOVE_RIGHT) 
					state = State.STAY_RIGHT;
				else if  (state == State.STAY_RIGHT) 
					state = State.MOVE_LEFT;
				else if  (state == State.STAY_LEFT) 
					state = State.MOVE_RIGHT;
			}
			if (state == State.MOVE_LEFT && x < 20)
				state = State.MOVE_RIGHT;
			if (state == State.MOVE_RIGHT && x > ground.width - 20)
				state = State.MOVE_LEFT;

			if  (state == State.MOVE_LEFT) 
				x -= vitesse;
			else if  (state == State.MOVE_RIGHT) 
				x += vitesse;
		}
}

