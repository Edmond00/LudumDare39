package gmtk;

import java.util.ArrayList;
import java.util.List;
import processing.core.*;
import ddf.minim.*;

public class Game extends PApplet{

		Minim minim;
		ImageBank bank;
		SoundBank sounds;
		static Game app = null;
		List<Object> lo;
		List<Screen> ls;
		Screen actualScreen;
		Rect screen;
		MainScreen main;

		PFont font;

		public void printText(String str, int size, int x, int y, int r, int g, int b)
		{
			fill(r, g, b);
			textSize(size);
			text(str, x, y);
		}

		public void centerText(String str, int size, int x, int y, Color color)
		{
			int w = (int)textWidth(str);
			int cx = x - w/2;
			int cy = y - size/2;
			fill(color.R, color.G, color.B);
			textSize(size);
			text(str, cx, cy);
		}

		public Rect textRect(String str, int size, int x, int y)
		{
			textSize(size);
			int w = (int)textWidth(str);
			return new Rect(x, y, w, size);
		}

		public void drawMark(int x, int y, int h, Color c) {
			fill(c.R, c.G, c.B);
			stroke(c.R, c.G, c.B);
			line(x, y, x, y+h);
		}

		public void drawLine(int x, int y, int x2, int y2, int w, Color c) {
			strokeWeight(w);
			fill(c.R, c.G, c.B);
			stroke(c.R, c.G, c.B);
			line(x, y, x2, y2);
			strokeWeight(1);
		}

		public void drawRect(Rect r, Color c) {
			fill(c.R, c.G, c.B);
			stroke(c.R, c.G, c.B);
			rect(r.x, r.y, r.w, r.h);
		}
		public void drawRect(Rect rect, int r, int g, int b) {
			fill(r,g,b);
			stroke(r,g,b);
			rect(rect.x, rect.y, rect.w, rect.h);
		}

		public void drawRect(int x, int y, int L, Color c) {
			fill(c.R, c.G, c.B);
			stroke(c.R, c.G, c.B);
			rect(x-L/2, y-L/2, L, L);
		}

		public void background(int r, int g, int b) {
			fill(r, g, b);
			stroke(r, g, b);
			rect(0, 0, screen.w, screen.h);
		}

		public void background(Color c) {
			fill(c.R, c.G, c.B, c.alpha);
			stroke(c.R, c.G, c.B, c.alpha);
			rect(0, 0, screen.w, screen.h);
		}

		public void log(String str) {
			System.out.println(str);
		}

		public static void main(String[] args) {
				PApplet.main("gmtk.Game");
		}

		public void settings(){
				app = this;
				screen = new Rect(0,0,1000,700);
				size(screen.w, screen.h);
		}

		public void setup(){
			minim = new Minim(this);
			bank = new ImageBank();
			sounds = new SoundBank();
			sounds.loop("rain");
			sounds.mute("rain");
			lo = new ArrayList<Object>();
			ls = new ArrayList<Screen>();
			main = new MainScreen(this, "main");
			new GameScreen(this, "game");
			new HelpScreen(this, "help");
			changeScreen("main");

			font = loadFont("./data/font/SansSerif-48.vlw");
			textFont(font);
			textAlign(LEFT, TOP);
		}

		public void draw(){
				actualScreen.drawComposant();
		}

		public void keyPressed() {
			if (key == 27) {
				main.setGameOver(false);
				changeScreen("main");
				key = 0;
			}
			if (keyPressed)
			{
				if (key == CODED)
					actualScreen.transmitCodedEvent(keyCode);
				else
					actualScreen.transmitAsciiEvent(key);
			}
		}

		public void changeScreen(String name)
		{
			if (name.equals("game"))
				sounds.unmute("rain");
			else
				sounds.mute("rain");
			for (Screen screen : ls) {
				if (screen.name.equals(name))
					actualScreen = screen;
			}
		}

}
