package gmtk;

import processing.core.*;
import java.util.LinkedList;
import java.util.ArrayList;
import java.util.List;
import java.util.ListIterator;

public class Animation {

	int slow;
	int i;
	int c;
	ArrayList<String> frames;

	Animation(int slow) {
		this.slow = slow;
		this.i = 0;
		this.c = 0;
		frames = new ArrayList<String>();
	}

	void add(String f) {
		frames.add(f);
	}

	void display(PImage img, int x, int y) {
		c++;
		if (img != null)
			Game.app.image(img, x, y);
		if (c > slow) {
			c = 0;
			i++;
			if (i >= frames.size())
				i = 0;
		}
	}

	void XcenterYdown(int x, int y) {
		PImage img = Game.app.bank.get(frames.get(i));
		if (img == null)
			return ;
		int px = x - img.width/2;
		int py = y - img.height;
		display(img, px, py);
	}

	void XcenterYcenter(int x, int y) {
		PImage img = Game.app.bank.get(frames.get(i));
		if (img == null)
			return ;
		int px = x - img.width/2;
		int py = y - img.height/2;
		display(img, px, py);
	}
}
