package gmtk;

import processing.core.*;

public class Pig extends Animal{

		public Pig(Ground g, int x, int y) {
			super(g, x, y);
			vitesse = 1;
			name = "pig";
			move = new Animation(20);
			move.add("pig1");
			move.add("pig2");
			rmove = new Animation(20);
			rmove.add("rpig1");
			rmove.add("rpig2");
			rstay = new Animation(20);
			rstay.add("rpig1");
			stay = new Animation(20);
			stay.add("pig1");
		}
		
		void draw() {
//			Game.app.drawRect(getPositionX(), getPositionY(), 10, Color.RABBIT);
			drawAnimation();
		}
}

