package gmtk;

import processing.core.*;
import java.util.ArrayList;
import java.util.List;

public class MainScreen extends Screen {

		Text title;
		Text gameover;
		List<Text> text;
		int select;

		public void update(int s)
		{
			if (s < 0 || s >= text.size())
				return ;
			select = s;
			for (Text t : text) {
				t.changeColor(Color.LABEL);
			}
			text.get(select).changeColor(Color.SLABEL);
		}

		public MainScreen(Game a, String n) {
			super(a, n, true);
			select = 0;

			text = new ArrayList<Text>();

			text.add(new Text(this, 50,60, "PLAY", 20));
			text.add(new Text(this, 50,70, "HELP", 20));
			text.add(new Text(this, 50,80, "QUIT", 20));
			title = new Text(this, 50,25, "HUNGRY SKY", 50, Color.THUNDER);
			gameover = new Text(this, 50,25, "GAME OVER", 50, Color.TITLE);
			gameover.hide = true;
			update(0);
		}

		public void setGameOver(boolean go) {
			if (go == false) {
				gameover.hide = true;
				title.hide = false;
			} else {
				gameover.hide = false;
				title.hide = true;
			}
		}

		public boolean asciiEvent(char key) {
			if (key == '\n') {
				if (select == 2)
					app.exit();
				else if (select == 0)
					app.changeScreen("game");
				else if (select == 1)
					app.changeScreen("help");
			}
			return true;
		}

		public boolean codedEvent(int key) {
			if (key == app.UP)
				update(select - 1);
			else if (key == app.DOWN)
				update(select + 1);
			return true;
		}
}
