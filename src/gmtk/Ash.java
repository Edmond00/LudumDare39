
package gmtk;

import processing.core.*;

public class Ash extends Item {
	

	Ash(Ground g, int x) {
		super(g, x, Game.app.screen.h - Ground.heigth);
		this.name = "ash";
	}

	void draw() {
		PImage img;
		img = Game.app.bank.get(name);
		int px = getPositionX() - img.width/2;
		int py = y;
		Game.app.image(img, px, py);
	}
}

