package gmtk;

import processing.core.*;
import java.util.LinkedList;
import java.util.ArrayList;
import java.util.List;
import java.util.ListIterator;

public class Avatar {

	public enum State {
		MOVE_LEFT(),
		MOVE_RIGHT(),
		STAY_LEFT(),
		STAY_RIGHT();

		State() {}
	}

	Animation move;
	Animation rmove;
	Animation swim;
	Animation rswim;
	Animation stay;
	Animation rstay;
	Animation ss;
	Animation rss;
	State state;
	int x;
	boolean died;

	Avatar (int x) {
		this.x = x;
		died = false;
		state = State.STAY_RIGHT;
		move = new Animation(8);
		move.add("move1");
		move.add("move2");
		rmove = new Animation(8);
		rmove.add("rmove1");
		rmove.add("rmove2");
		swim = new Animation(12);
		swim.add("swim1");
		swim.add("swim2");
		rswim = new Animation(12);
		rswim.add("rswim1");
		rswim.add("rswim2");
		rstay = new Animation(8);
		rstay.add("rmove2");
		stay = new Animation(8);
		stay.add("move2");
		rss = new Animation(12);
		rss.add("rswim1");
		ss = new Animation(12);
		ss.add("swim1");
	}

	void draw() {
		int x = Game.app.screen.w/2;
		int y = Game.app.screen.h - Ground.heigth;
		if (!swimming()) {
			if (state == State.MOVE_LEFT)
				rmove.XcenterYdown(x, y);
			if (state == State.MOVE_RIGHT)
				move.XcenterYdown(x, y);
			if (state == State.STAY_LEFT)
				rstay.XcenterYdown(x, y);
			if (state == State.STAY_RIGHT)
				stay.XcenterYdown(x, y);
		} else {
			if (state == State.MOVE_LEFT)
				rswim.XcenterYcenter(x, y);
			if (state == State.MOVE_RIGHT)
				swim.XcenterYcenter(x, y);
			if (state == State.STAY_LEFT)
				rss.XcenterYcenter(x, y);
			if (state == State.STAY_RIGHT)
				ss.XcenterYcenter(x, y);
		}
	}

	void update() {
		if (Ground.here == x) {
			if (state == State.MOVE_LEFT)
				state = State.STAY_LEFT;
			if (state == State.MOVE_RIGHT)
				state = State.STAY_RIGHT;
		}
		x = Ground.here;
	}

	boolean swimming() {
		if (Ground.inHere.is("water") == false)
			return false;
		if (x - Ground.inHere.begin < 5)
			return false;
		if (Ground.inHere.end - x < 5)
			return false;
		return true;
	}
}
