
package gmtk;

import processing.core.*;

public class Fish extends Animal{

		public Fish(Ground g, int x, int y) {
			super(g, x, y);
			vitesse = 2;
			name = "fish";
			move = new Animation(8);
			move.add("fish");
			rmove = new Animation(8);
			rmove.add("rfish");
			rstay = new Animation(8);
			rstay.add("rfish");
			stay = new Animation(8);
			stay.add("fish");
		}
		
		void draw() {
			//Game.app.drawRect(getPositionX(), getPositionY(), 10, Color.FISH);
			drawAnimation();
		}
}

