package gmtk;

import processing.core.*;
import java.util.ArrayList;
import java.util.List;

abstract class Screen extends Object {

		String name;

		public Screen(Game a, String n, boolean firstScreen) {
			super(a, a.screen.x, a.screen.y, a.screen.w, a.screen.h);
			name = n;
			app.ls.add(this);
			if (firstScreen)
				app.actualScreen = this;
		}

		public void draw() {
			app.background(20,20,20);
		}
}
