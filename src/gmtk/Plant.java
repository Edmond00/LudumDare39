
package gmtk;

import processing.core.*;

public class Plant extends Item {
	
	String burnName;
	boolean burned;
	int width;

	Plant(Ground g, int x, String name, String burnName, int width) {
		super(g, x, Game.app.screen.h - Ground.heigth);
		this.width = width;
		this.burnName = burnName;
		this.burned = false;
		this.name = name;
	}

	void draw() {
		PImage img;
		if (burned)
			img = Game.app.bank.get(burnName);
		else
			img = Game.app.bank.get(name);
		int px = getPositionX() - img.width/2;
		int py = y - img.height;
		Game.app.image(img, px, py);
	}

	void struck(int tbx) {
		if (Ground.contact(tbx, getX(), width))
			burned = true;
	}
}

