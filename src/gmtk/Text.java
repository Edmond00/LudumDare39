package gmtk;

import processing.core.*;

public class Text extends Element {

		String str;
		Integer size;
		Integer r, g, b;

		public Text(Object p, int x, int y, String astr, int asize, Color color) {
			super(p, p.app.textRect(astr, asize, 0, 0));
			relativeCenter(x, y);
			str = astr;
			size = asize;
			r = color.R;
			g = color.G;
			b = color.B;
		}

		void changeColor(Color color)
		{
			r = color.R;
			g = color.G;
			b = color.B;
		}

		public Text(Object p, int x, int y, String astr, int asize) {
			this(p, x, y, astr, asize, Color.MAIN);
		}

		public void draw() {
			app.printText(str, size, rect.x, rect.y, r, g, b);
		}
}
