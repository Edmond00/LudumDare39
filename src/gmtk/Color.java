package gmtk;

public enum Color {
		 COUNTDOWN_MARK(255, 0, 0),
		 COUNTDOWN_BACK(100, 50, 25),
		 COUNTDOWN_BORDER(0, 0, 0),
		 COUNTDOWN_FRONT(255, 255, 0),
		 THUNDER(255, 255, 150),
		 AVATAR(255, 100, 100),
		 FISH(0, 0, 200),
		 RABBIT(255, 255, 255),
		 FOOD(255, 0, 255),
		 HERE(255, 200, 200),
		 EARTH1(100, 35, 15),
		 EARTH2(80, 20, 10),
		 EARTH3(40, 10, 0),
		 WATER1(240, 240, 255),
		 WATER2(200, 200, 255),
		 WATER3(50, 50, 200),
		 WATER4(0, 0, 150),
		 GROUND(35, 130, 5),
		 LABEL(120, 120, 120),
		 SLABEL(255, 255, 255),
		 TITLE(220, 220, 220),
		 MAIN(100, 100, 100),
		 WINDOW(20, 20, 20),
		 SWINDOW(5, 5, 5),
		 SBORDER(240, 240, 240),
		 SHADOW(0, 0, 0, 180),
		 CLOUD(85, 85, 85, 180),
		 RAIN(50, 100, 200),
		 RAIN2(220, 220, 255),
		 BORDER(150, 150, 150);

		public int R;
		public int G;
		public int B;
		public int alpha;

		//Constructeur
		Color(int r, int g, int b){
				this.R = r;
				this.G = g;
				this.B = b;
				this.alpha = 255;
		}
		Color(int r, int g, int b, int a){
				this.R = r;
				this.G = g;
				this.B = b;
				this.alpha = a;
		}
}
